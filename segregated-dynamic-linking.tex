\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{style.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[hidelinks]{hyperref} % needed for URLs and the glossary
\hypersetup{colorlinks=false}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[nomain,toc,nonumberlist,acronym,style=altlist]{glossaries}
\renewcommand{\glstextformat}[1]{{\color{darkgray!70!blue} #1}}
\makenoidxglossaries
\input{definitions.tex}
\makeindex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% don't mash _ into subscript quite so hard:
\chardef\_=`_

\begin{document}
\pagestyle{collabora}
\title{Segregated Dynamic Linking with ELF}
\author{Vivek Das Mohapatra}
\authoruri{mailto:vivek@collabora.com}
\copydates{2017, 2018}
\copyholder{\www{https://collabora.com/}{Collabora Ltd}}
\maketitle{}
\section*{Introduction}
{\itshape
In this article I'm going to discuss a project I've been working on at
my employer (\www{https://www.collabora.com/}{Collabora Ltd}) for one of our
customers (\www{http://www.valvesoftware.com/}{Valve Software}): Segregated
Dynamic Linking for ELF binaries.}

\tableofcontents
\clearpage

\glsaddall
\section{Terminology}
% scrub the glossary title entirely, it screws up the multicolumn layout:
\renewcommand{\glossarysection}[2][]{}
\begin{cartouche}
\begin{multicols}{2}[]
\printnoidxglossary
\end{multicols}
\end{cartouche}
\clearpage
\begin{multicols}{2}[%
\section{What is Segregated Dynamic Linking?}]
%\section{What is Segregated Dynamic Linking?}

In normal dynamic linking there is one copy of every \gls{DSO} mapped
into memory, and each \gls{DSO} can see the symbols of every \gls{DSO}
\emph{that it asked to see}.

Example: If my program needs two \glspl{DSO} (libAA and libBA) and
both the program and libBA need a third \gls{DSO} (libBB) then there
will be one copy of libBB and both the main \gls{DSO} and libBA will
see libBB's symbols, but libAA will not.

\begin{center}
\input{vanilla-dynamic-link-diagram.tex}
\end{center}

In Segregated Dynamic Linking, we still link in a \gls{DSO} as usual
but we hide some of the symbols (functions, variables, etc) that would
normally be visible as a result: This allows us to limit the
visibility of symbols that would otherwise cause conflicts.

\begin{center}
\input{segregated-dynamic-link-diagram.tex}
\end{center}
\end{multicols}

\begin{multicols}{2}[%
\section{\emph{Why} is Segregated Dynamic Linking?}]

It would be perfectly reasonable, at this point, to ask why we don't ‘just’
update the offending library to use a compatible version of its dependency:

In an ideal world that is exactly what we'd do - unfortunately that is not
the world in which we live. 

In this particular case the scratch that needs itching is the ability of a 
game in a runtime (to oversimplify: a fixed set of libraries guaranteed to 
be at specific versions) to work with a Mesa (3D graphics library) pulled
from outside the runtime.

Why is this necessary? It falls out like this:

\columnbreak

The runtime is effectively a promise made to the game developers that certain
versions of a given set of libraries will always be available, no matter
which version of the host operating system the game finds itself on.

But Mesa needs to talk to specific hardware - and hardware changes rapidly.
So we can't drop a frozen-in-time version of Mesa into the runtime and use
that: Even though the Mesa interface might not change the old Mesa probably
wouldn't be able to talk to shiny new hardware - which would mean your game
simply wouldn't run on hardware made any significant amount of time
after its release.

Ok, you might reasonably ask, why not ‘just’ pull in the Mesa from outside
the runtime and be done with it? It turns out this is where our troubles
begin:

\clearpage

\begin{itemize}
\item Mesa (or rather its hardware drivers) depend on {\tt{libstdc++}}
\item The runtime provides a specified {\tt{libstdc++}} version
\item Our game loads the runtime {\tt{libstdc++}} \gls{DSO}
\item Our game loads the Mesa \glspl{DSO}
\item The \gls{linker} notes that it already has a {\tt{libstdc++}} \gls{DSO}
\item The \gls{linker} uses this \gls{DSO} to link the Mesa \glspl{DSO}
\item If the versions aren't compatible… we crash 💢
\end{itemize}

This is not, as you can imagine, a great sales pitch for your runtime:
You can work around it by carefully controlling both the host operating
system \emph{and} the runtime - but that limits your portability.

That, then, is the initial problem. Our solution will look something
like this:

\input{host-runtime-mesa-diagram.tex}
\end{multicols}

\section{How do we achieve segregated linking?}

To answer that question we're going to need to pay attention to
some low-level details of how function calls between \glspl{DSO} work:

\subsection{Calling foreign functions from a DSO}

When code in a \gls{DSO} wishes to call a \gls{foreign function} it
cannot do so directly: It has no way of knowing where in memory the
linker has put the other \gls{DSO}. This is fixed (as with all problems)
by adding a layer of indirection (actually two, because it makes things
tidier). Here's how it works:

{\bfseries Some definitions:}

\begin{cartouche}
\begin{multicols}{2}[]
\begin{description}[style=nextline]
  \item[PLT - Procedure Linkage Table]

    An array of stub functions, one for each foreign function.

    The \gls{PLT} resides in the Read-Only TEXT section of the \gls{DSO},
    along with the rest of the executable code, and is shared
    by all copies of the  \gls{DSO} in use.

    Every \gls{DSO} has its own \gls{PLT}.
    \columnbreak

  \item[GOT - Global Offset Table]

    An array of \glspl{RR}, each giving the actual location of
    an item mentioned in the \gls{PLT}.

    The \gls{GOT} resides in the read-write DATA section of the \gls{DSO},
    there is a private copy for each copy of the \gls{DSO} in use.

    Every \gls{DSO} has its own \gls{GOT}.
    \columnbreak

\end{description}
\end{multicols}
\end{cartouche}

\clearpage

This is how the first call to a \gls{foreign function} (whose address we do 
not yet know) looks: \vspace{-0.8\baselineskip}

\newcommand{\ffmaybe}[1]{\item[\bang{green}] \color{cgreen}{— #1}}
\newcommand{\ffevery}[1]{\item[\bang{blue}] \color{cpurple}{#1}}

\begin{itemize}[itemsep=0.2\baselineskip]
  \ffevery{Calling code puts \gls{foreign function} arguments on the stack}
  \ffevery{Excution jumps to a fixed offset in the \gls{PLT} (specific
           to this function)}
  \ffevery{The \gls{PLT} stub looks up the corresponding address in the
           \gls{GOT}} and jumps to it
  \ffmaybe{The fixup code pointed at by the \gls{GOT} asks the linker for the
           real address}
  \ffmaybe{The linker searches the calling \glspl{DSO} dependencies
           for the \gls{symbol}}
  \ffmaybe{The fixup code writes the address into the \gls{GOT} slot}
  \ffmaybe{The fixup code jumps to the address in the \gls{GOT} slot}
  \ffevery{The code in the foreign \gls{DSO} pulls the arguments off the stack}
  \ffevery{The return value is pushed onto the stack}
  \ffevery{Execution jumps back to the original \gls{DSO}, just after the
           original call}
\end{itemize}

\input{foreign-function-call-uml-diagram.tex}

%\vspace*{1.0\baselineskip}

Subsequent calls skip the stages marked {\color{cgreen}— (in green)} as the \gls{GOT}
slot will already contain the real symbol's address.

\clearpage
Drawing that out in greater detail:
\input{foreign-function-call-diagram.tex}

\begin{multicols}{2}[Some things worth noting here (they'll be important later):]

\begin{itemize}
\item Only the calling code in the \gls{DSO} and the called code in
  the foreign \gls{DSO} know anything about the arguments and return
  value (signature) of the called function. None of the intervening
  steps know or care about these things.

\item The \gls{PLT} is read-only, but the \gls{GOT} is writable.

\item The address stored in the \gls{GOT} slot is the only thing we need to
  change to divert a function call.

\item If we scribble an address into the \gls{GOT} slot before the fixup
  code is ever called we can completely short circuit the \gls{linker}'s
  normal symbol search algorithms.
\end{itemize}
\end{multicols}
\clearpage

\begin{multicols}{2}[\subsection{How the linker finds DSOs}]

The observant reader may have noticed that we have glibly skimmed over
some details above: “The \gls{linker} searches the \glspl{DSO} that the 
calling \gls{DSO} depends on”

{\itshape Let's unpack some of those details:}

When the \gls{linker} opens a \gls{DSO} for the first time it scans it for 
items in the ‘Dynamic Section’. There are many types of entry here, but for
now we're interested in the {\tt{DT\_NEEDED}} entries. These entries tell the
\gls{linker} which \glspl{DSO} are needed by the initial \gls{DSO}.

\columnbreak

The other piece of infrastructure we need to know about is the
‘{\color{violet}\bf{link map}}’ list. A link map list is a [doubly]
linked list of currently loaded \glspl{DSO} - when a \gls{DSO} is linked
its unresolved \glspl{symbol} are searched for in the link map list,
but only \glspl{DSO} mentioned explcitly in its {\tt{DT\_NEEDED}} entries, 
plus any \glspl{DSO} which have been declared as globally available,
are used for this search: Others are skipped over.

A simplified diagram showing a 3-element link map list:

\vspace{-1em}
\begin{center}
\input{link-map-diagram.tex}
\end{center}

\end{multicols}

\hrule{}

\begin{multicols}{2}[{\itshape Here's the rough process: }\normalfont]

\begin{itemize}
\item Open the target \gls{DSO}
\item Scan it for {\tt{DT\_NEEDED}} entries
\item For each dependency:
  \begin{itemize}
  \item If the \gls{DSO} is already mapped add its link map entry to
        the link map list for our target
  \item Otherwise: 
    \begin{itemize}
    \item open the new \gls{DSO}
    \item map it into memory
    \item add the new link map entry to the list for our target
    \item recurse into this algorithm for the new \gls{DSO} 
    \end{itemize}
  \end{itemize}
\item Add any declared-global \glspl{DSO} to the target's link map list
\end{itemize}

\end{multicols}

\hrule{}

\begin{multicols}{2}[]

NOTE: the {\tt{DT\_NEEDED}} entry is generally the bare \gls{DSO} name -
something like {\color{violet}{\tt{libfoo.so.1}}} - and when checking
the link map for a \gls{DSO} it is this last part of the path that is
compared. The exact path does not normally matter.

When the \gls{linker} searches for a \gls{symbol} for a given \gls{DSO}, 
it searches only in the link map for the requesting \gls{DSO}. (A given 
\gls{DSO} can appear in multiple link maps, the same copy is re-used).

Now we understand how the linker searches for symbols - we've learned
some important things:

\begin{itemize}
  \item We have the foundations of restricted symbol visibility

  \item The key will be controlling which \glspl{DSO} appear in our link map

  \item We can prevent a \gls{DSO} from being loaded by pre-seeding a 
        \gls{DSO} with the same base name in the current link map
\end{itemize}

This looks pretty promising - we have all (or nearly all) the pieces of the
puzzle. We might, in fact, have all the pieces if we're willing to get
our hands very dirty indeed and hack up the link maps entirely by hand…
But it turns out the glibc developers at \www{https://www.redhat.com/}{Red Hat}
have already done some of the heavy lifting for us in the form of the
\texttt{dlmopen()} call.
\end{multicols}

\begin{multicols}{2}[\subsection{Introducing dlmopen()}]

First some background: The {\tt{dlopen()}} C library call opens a
\gls{DSO}, in a similar way to the \gls{linker} at program start up time:
It opens the target and its {\tt{DT\_NEEDED}} dependencies (in the
same way as the \gls{linker} does, described above) and adds them to the
link map for the new \gls{DSO}.

This is close to what we want, but not quite there for two main reasons:

\begin{itemize}

\item {\tt{dlopen()}} will re-use existing link map entries which match the
      target \gls{DSO}'s {\tt{DT\_NEEDED}}, which might pull in the ‘wrong’
      version of a \gls{DSO}.

\item If a new \gls{DSO} is later opened and flagged as global,
      and our \gls{DSO} (or any of its {\tt{DT\_NEEDED}} entries) are also
      required by that new \gls{DSO}, they will cease to be hidden and
      start being used by the \gls{linker} (and we want to control \gls{DSO}
      and \gls{symbol} visibility).

\end{itemize}

This is where {\tt{dlmopen()}} comes in: It is like {\tt{dlopen()}},
but puts the new \gls{DSO}'s link map entries in a specific link
map list (that you request) or creates a new list and puts the entries
there.

If the new \gls{DSO} mapping is not added to the default link map
list the \gls{linker} won't encounter it while resolving \glspl{symbol}
for \glspl{DSO} in the default namespace - this means no \glspl{symbol}
from the new \gls{DSO} will be exposed or used during normal linking.

Likewise since the new \gls{DSO} gets its own private link map list,
it won't (by default) have any of its \glspl{symbol} resolved from
our main set of \glspl{DSO}.

Now that we have all\footnote{Spoiler: We're going to need more pieces.} the 
pieces of the puzzle, we can put together a solution to our problem.

\end{multicols}
\clearpage

\section{Putting the pieces together}

\begin{multicols}{2}[Let's start by stating the exact problem we're going to try and solve:]

\begin{itemize}
\item We have a runtime in which we wish to start a program
\item The runtime contains some, but not all of the \glspl{DSO} we need
\item The runtime appears to the program as a filesystem starting at {\tt{/}}
\item The real filesystem is bound into the runtime, and appears as {\tt{/host}}
  \begin{itemize}
  \item In other words, {\tt{/lib}} from the OS appears as {\tt{/host/lib}}, and so on.
  \end{itemize}
\item If a \gls{DSO} comes from {\tt{/host}}, we want to use only other
      \glspl{DSO} from {\tt{/host}}
\item Only the explicitly required {\tt{/host}} \gls{DSO} should be used by 
      the \gls{linker}
  \begin{itemize}
  \item ie anything else pulled in from {\tt{/host}} should remain invisible to
        the program and other runtime \glspl{DSO}
  \end{itemize}
\item The program should run completely unaltered: 
      No rebuilds, no patching of the binary.
\end{itemize}

\end{multicols}

\begin{multicols}{2}[\subsection{Supplying a \emph{shim} DSO}]

The first step is allowing the program to find a \gls{DSO} that satisfies
its {\tt{DT\_NEEDED}} entry. We can't simply add the {\tt{/host}} tree to the 
\gls{linker}'s search path (that wouldn't allow {\tt{/host}} vs runtime 
segregation): We can, however, provide a fake library with the same name 
as our target \gls{DSO} and let the \gls{linker} find that.

This shim library is made easier to generate by the fact that we don't
(as you may recall from earlier) need to know the signature of each
function: We're never going to call the stubroutines it contains so 
we only need then to have a compatible entry in the symbol table of 
our shim \gls{DSO}.

\end{multicols}

\begin{multicols}{2}[\subsection{Loading and exposing the real symbols}]

Now that we've generated our fake \gls{DSO}, we need to have it find
the real \glspl{symbol} and patch up the \glspl{GOT} of any \glspl{DSO}
that need them.

This requires a few steps:

\begin{itemize}
\item The list of \glspl{symbol} to export from the {\tt{dlmopen()}} namespace
      is built into the library (it turns out this is the most reliable
      way to do this - and there's no reason not to other than code/binary
      size)

\item The shim \gls{DSO} harvests the {\tt{DT\_NEEDED}} entries recursively from
      the target \gls{DSO} from {\tt{/host}} and its dependencies, keeping track
      of which needs what

   \begin{itemize}
   \item To find the relevant \glspl{DSO}, our shim must read and interpret
         the \gls{linker} cache at {\tt{/host/etc/ld.so.cache}}, as well as searching
         {\tt{LD\_LIBRARY\_PATH}}, if set, and also searching the default locations
         at {\tt{/lib}} and {\tt{/usr/lib}}
   \item In addition to the path mapping we also have to be careful
         to ignore \glspl{DSO} with the ‘wrong’ type - on x86* there are
         three common types: x86-64, i386 and x32. We only want to
         pick \glspl{DSO} which are the same as the original \gls{DSO} (the program)

   \item While it does so it takes care to remap all paths to have a {\tt{/host}}
         prepended to them, including resolving any symlinks manually
         with the extra {\tt{/host}} element where necessary

   \item Some \glspl{DSO} should not be pulled from {\tt{/host}} - notably
         the \gls{linker} ({\tt{ld.so}}) itself. It also turns out that in
         order for the program to work reliably the libc cluster (the core
         \tt{C library}, \tt{libpthread} and so on) must also be from the
         same tree as one another (and ∴ not from /host)
   \end{itemize}

\item It then {\tt{dlmopen()}}s the target \glspl{DSO} in \emph{reverse} dependency 
      order: That is - Any \glspl{DSO} with no dependencies are opened first,
      then the \glspl{DSO} which only needed those first \glspl{DSO}, and so 
      on and so forth

   \begin{itemize}
   \item The {\tt{dlmopen()}} calls are made with the full {\tt{/host/…}} path to the
         \gls{DSO} in question, to make sure we pick up the {\tt{/host}} version

   \item The linker, when checking for {\tt{DT\_NEEDED}} items in the link map,
         only checks the base name of the \gls{DSO} (the final 
         {\color{violet}{\tt{libfoo.X}}} part) so our {\tt{/host}} \glspl{DSO} only 
         get other {\tt{/host}} \glspl{DSO} used to satisfy their requirements.
   \end{itemize}

\item Finally, once the full {\tt{/host}} \gls{DSO} set has been loaded, our shim
      \gls{DSO} monkeypatches the runtime \glspl{DSO} to make them use the
      right \glspl{symbol} (this is complicated enough that I'm going to break
      it out into its own section [see below]).
\end{itemize}

\end{multicols}

\subsection{Monkeypatching a symbol into a DSO}
\vspace{-1.0em}
\begin{multicols}{2}[\subsubsection{ELF DSO layout in memory}]

This is where we need to dive into the details of how an \gls{ELF} 
\gls{DSO} is laid out in memory (as opposed to on disc) in even 
greater detail.

They key to all of this is the glibc library function 
{\tt{dl\_iterate\_phdr()}}: it allows us to invoke a callback function of
our choice on each \gls{DSO} in the \emph{default} namespace (it does
\emph{not} touch private {\tt{dlmopen()}} namespaces).

Our callback function will know the base address in memory of the
\gls{DSO}, the name of the \gls{DSO}, and the start of the array of
\gls{ELF} Program Headers.

There are many types of program header (see {\tt{/usr/include/elf.h}})
but luckily for us we're only interested in {\tt{PT\_DYNAMIC}}
sections. These are, unsurprisingly, the part(s) of the \gls{ELF}
\gls{DSO} related to dynamic linking. There could technically be more
than one dynamic section: We're going to iterate over them all and
treat them all the same so this doesn't matter much.

In each {\tt{PT\_DYNAMIC}} section we find an array of {\tt{ElfW(Dyn)}}
entries, each with a {\tt{DT\_…}} type, finishing with an entry of type
{\tt{DT\_NULL}} (see {\tt{elf.h}} again).

These dynamic entries are not guaranteed to be in any particular
order (although the {\tt{DT\_*TAB}} entries will appear at the start),
and some of them can only be interpreted usefully by referring to
the contents of other {\tt{DT\_…}} entries. Here are the ones we turn out to
be interested in:

\end{multicols}

\hrule{}

\begin{multicols}{2}[]

\begin{description}[style=nextline]

  \item[DT\_STRTAB]

    The string table. This points to an area (within the current
    {\tt{PT\_DYNAMIC}} section) that contains {\tt{NUL}} terminated strings.
    Any {\tt{DT\_…}} item that needs to refer to a printable string
    will point to a location in this entry

    (pointer)

  \item[DT\_SYMTAB]

    The \gls{symbol} table. Contains some metadata about each symbol such
    as its visibility and type. Since we're working from a pre-compiled
    list we can assume most of these - we're mainly interested as a
    way of finding the name of the symbol in the {\tt{DT\_STRTAB}}

    (pointer)

  \item[DT\_PLTREL]

    This gives the type of the relocation table - {\tt{DT\_REL}} or {\tt{DT\_RELA}}

    (integer)

  \item[DT\_PLTRELSZ]

    The total size of the array of relocation records in the 
    relocation table. Used for bounds-checking.

    (integer)

  \item[DT\_RELASZ/DT\_RELSZ]

    The size of the {\tt{DT\_REL[A]}} relocation table.
    At most one may be present (and must match the table type).

    (integer)

  \item[DT\_JMPREL]

    The address of the relocation records. We need {\tt{DT\_PLTREL}}
    \& {\tt{DT\_PLTRELSZ}} to process this.

    (pointer)

  \item[DT\_REL/DT\_RELA]

    In addition to the relocation table pointed at by {\tt{DT\_JMPREL}}, there
    may also be additional relocation tables existing independently.
    They're typically not interesting as all function-related relocations
    should be in the {\tt{DT\_JMPREL}} table, but we process them anyway as this
    layout rule is ‘just’ a convention.

    (pointer, not seen in testing so far)
\end{description}

\end{multicols}

\subsubsection{Performing a manual symbol relocation}

\begin{multicols}{2}[Having found the symtab, strtab and relocation
  array, we can process the entries there: Each relocation record
  contains the following information:]

\begin{itemize}
\item The address (of the \gls{GOT} entry, not the \gls{symbol})
      That is, the address of the address used by the \gls{PLT} to call
      the function

\item The relocation type. There are \emph{many} relocation types 
      (about 40 for x86-64 alone). Fortunately to relocate basic function
      calls we only need to handle a few per architecture:
      {\tt{R\_X86\_64\_JUMP\_SLOT}} \& {\tt{R\_386\_JMP\_SLOT}} being the
      most important - and occasionally {\tt{R\_X86\_64\_GLOB\_DAT}},
      {\tt{R\_X86\_64\_64}} and their {\tt{R\_386…}} equivalents.

      [ {\itshape Every relocation type has its own rules for how to calculate the
        correct jump.} ]

\item The addend (an additional offset to the address). This occurs if the
      item being relocated is an offset from another named object (eg if
      a variable is defined as a fixed offset into an array or struct, and
      the compiler knows this).

      [ Only {\tt{DT\_RELA}} records have addends. ]

\item The index (0-based) of this \gls{symbol} in the symtab.
\end{itemize}

\end{multicols}

\begin{multicols}{2}[Finally, having gathered all this information, we can perform a relocation
(remember that we have to do this for every \gls{DSO} from the runtime as each
one will have its own \gls{GOT}):]

\begin{itemize}
\item Check the name of the \gls{symbol} (relocation record → symtab → strtab)
      against the hard-coded relocation list in our shim \gls{DSO}.
      If it's not there we're leaving this \gls{symbol} alone.

\item Get the real address of the \gls{symbol} from the {\tt{dlmopen()}} private
      namespace with {\tt{dlsym()}}.

\item Calculate the address of the \gls{GOT} slot which we wish to overwrite.
      To start with, it will contain the address of the fixup code in the
      \gls{PLT}.

\item Write the real address into the \gls{GOT} slot.

\item Proceed to beer island. We have monkeypatched one function symbol
      in one \gls{DSO}!
\end{itemize}

What's left is repeating this process for every \gls{symbol} in our 
hard-coded export list, for every \gls{DSO} from the runtime.

Strictly speaking we should only monkeypatch symbols in \glspl{DSO}
which require the \emph{real} library via a {\tt{DT\_NEEDED}} entry:
If we are a shim for \texttt{libfoo.so.1} then we should only try to
replace \glspl{symbol} in other \glspl{DSO} that explicitly depend on
\texttt{libfoo.so.1}. In practice it has not been necessary to be careful
about this (at least so far).

It's worth noting that we're using a very stupid, brute-force approach
to matching each \gls{symbol}: We don't use any of the clever hashing
that the \gls{linker} uses to speed things up. This is a deliberate ploy 
to keep the number of moving parts down to a minimum.

\end{multicols}

\begin{multicols}{2}[\section{Lies, TODOs, and FIXMEs…}]

That's it, we're done. Except… for the lies. I have, for the sake of
simplicity(!) told some lies and left out some details along the way. 
Here they are:

\begin{itemize}
\item Lie: The \gls{GOT} slot contains the fixup address to start with.
\item Lie: The \gls{GOT} slot is (always) writable.

    These are sort of true. Mostly. Except that if the library
    is {\tt{RELRO}} linked, the \gls{linker} will pre-emptively fix up the
    addresses before any code is invoked by the \gls{DSO} and then
    {\tt{mprotect()}} the memory region containing the \gls{GOT} to render it
    unwritable.

    The workaround is to read the {\tt{mprotect()}}ed region data from
    {\tt{/proc/self/maps}}, toggle the write bits on, do all our
    monkeypatching and then put them back the way they were.

\item TODO: The \gls{symbol} lookup has been simplified: I've left out all the 
      details to do with symbol versioning.

      The initial goal of the project doesn't require \gls{symbol} versioning
      support, so our shim \glspl{DSO} currently don't handle it. At all.

\item Lie: {\tt{dlopen()}} will work in the private namespace

      Currently {\tt{dlopen()}} cannot be called from any code
      that resides in a private {\tt{dlmopen()}} namespace.

      There are two reasons for this: The project-related one is that
      our target library ({\tt{libGL}}) uses {\tt{dlopen()}} to open its drivers…
      but it doesn't know it's been re-mapped to {\tt{/host}} and must get
      all its dependencies and modules from there.

      The workaround here is to monkeypatch all the DSOs \emph{inside} the
      private namespace to divert {\tt{dlopen}} to a wrapper that does all of
      the magic {\tt{dlmopen()}} reverse-dependency-remap-to-host magic.

      Fortunately \emph{most} of the pieces we need for this are ones 
      we've already had to implement for monkeypatching \glspl{DSO} from 
      the runtime, with one glaring exception: {\tt{dl\_iterate\_phdr()}} 
      doesn't deliver private namespace entries to us.

      However we're in luck - we can use {\tt{dlinfo()}} with
      {\tt{RTLD\_DI\_LINKMAP}} to get a link map entry instead, and 
      this turns out to have \emph{almost} all the information that 
      {\tt{dl\_iterate\_phdr}} gives us (and all the info that we care
      about).

      [{\itshape The other problem is that {\tt{dlopen()}} currently segfaults if called
       from inside a private {\tt{dlmopen}} namespace - this is a glibc 
       limitation}]

\item LIE: Monkeypatching only needs to happen at start-up

      If the program ever {\tt{dlopen()}}s a \gls{DSO} after start up, and it
      has our target \gls{DSO} in its {\tt{DT\_NEEDED}} entries, it will get 
      the fake addresses from our shim \gls{DSO}.

      The fix is to also wrap {\tt{dlopen()}} in the runtime
      libraries, replacing it with a wrapper that calls the real 
      {\tt{dlopen()}} and then fixes up any new \glspl{DSO} we haven't 
      seen before.

\item LIE: Monkeypatching is the only address-redirection we need to do

      If any of the runtime \glspl{DSO} use {\tt{dlsym()}} to find a symbol
      patched up by our shim library, they'll find the fake function instead.

      We wrap {\tt{dlsym()}} in the same way as {\tt{dlopen()}} aboveto handle this.

\item FIXME: {\tt{dlmopen() and RTLD\_GLOBAL}} don't work together

      {\tt{dlopen()}} supports {\tt{RTLD\_GLOBAL}}. {\tt{dlmopen()}} does not.
      If the {\tt{/host}} \glspl{DSO} need {\tt{RTLD\_GLOBAL}} {\tt{dlopen()}}
      calls to work then currently they… won't.

      This is another glibc limitation.

\item TODO: We only need to worry about a few relocation types

      There are more relocation types we may need to support for this
      project to be generically useful. Variables (which don't go
      through the \gls{PLT}) should be supported. Likewise TLS (Thread Local
      Storage) relocations will require more relocation type support.

      This is another item that isn't (I believe) particularly difficult,
      but it hasn't been necessary for the initial goals of our project,
      so onto the TODO list it goes. 

\pagebreak

\item LIE: The \gls{DSO} namespaces can be completely segregated

      It turns out that for boring implementation detail reasons, some
      \glspl{DSO} must not only be the same version but the same \textit{instance}
      in both the main and secondary namespaces.

      In particular the \texttt{C library} cluster cannot function reliably
      with more than one copy, particuarly if threading is activated at any time.

      This is not possible in any released \texttt{glibc} version, but
      patches implementing an \texttt{RTLD\_SHARED} dlopen flag have been
      tried (successfully) and sent upstream for review.
\end{itemize}
\end{multicols}

This, then, is what our solution ends up looking like:

\begin{center}
\input{encapsulated-mesa-diagram.tex}
\end{center}

There you have it: Everything you never wanted to know about
subverting the \gls{ELF} dynamic \gls{linker}.

\section{Postscript: Putting it all into practice}

To prove that all of this can actually be done the work-in-progress
repo, which has been successfully used to proxy \www{https://www.mesa3d.org/}{Mesa}’s \texttt{libGL}, can be found here:

\begin{center}
\www{https://gitlab.collabora.com/vivek/libcapsule}{\Large{}libcapsule.git}
\end{center}

\clearpage
\pagestyle{end}
\vspace*{0.75\textheight}
\begin{center}
\includegraphics[scale=0.53]{images/colicon-greyscale.eps} \\
\vskip 1.5em
\href{https://www.collabora.com/}{\whitefont\large{}www.collabora.com}
\end{center}
\end{document}
